---

## GWSGLWallpaperService

There are several examples of GLWallpaperService of robert Green out there but all relying on old code,
thus this is an update to source I found in blurred-lines and further modified to correct other issues.

Basically setGLWrapper is simplified, the EGLinterface factories are moved into separate files and
the interface to GLWrapper is put into the GLWallpaperService class.

## Credits

Robert Greent

[his site](http://rbgrn.net)

Alexander Fedora

Blurred-lines

